<?PHP 

    include_once("classes/Db.class.php");
    include_once("classes/Users.class.php");

    /*try {
        $v = new Users();
        $all = $v->GetAll();

            if (isset($all)){
                while($row = $all->fetch(PDO::FETCH_ASSOC))
                {
                    echo $row['email'];
                }
            }
    }
catch(Exception $e)
    {
        
    }*/

    if(!empty($_POST))
    {
        if(!empty($_POST['name'])&&!empty($_POST['email'])&&!empty($_POST['keuze']))
        {
            $v = new Users();
            $all = $v->GetAll();

            $exits = false;
            while($row = $all->fetch(PDO::FETCH_ASSOC))
            {
                if($row['email'] == $_POST['email'])
                {
                    $exits = true;
                }
            }

            if (!$exits)
            {
                $new_user = new Users();
                $new_user->Naam = $_POST['name'];
                $new_user->Email = $_POST['email'];
                $new_user->Optie = $_POST['keuze'];
                $new_user->Create();
                echo '<script>  alert("Uw keuze werd doorgegeven, bedankt!");  </script>';
            }
            else
            {
                echo '<script>  alert("Het ingegeven emailadres bestaat al in onze database");  </script>';
            }
        }
    }


?>

<!DOCTYPE html>
<html lang="nl">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1">
    <meta name="description" content="Girls in IMD campagne.">
    <meta name="keywords" content="girls, weareimd, graphic design, webdesign, webdevelopment">
    <meta name="author" content="Bryan, Niels, Stijn, Twix">

    <title>Girls in IMD</title>
    <link rel="icon" href="img/favicon.png">
    
    <!--OPENGRAPH TAGS-->
    <meta property="og:url" content="http://girlsinimd.nielsmeulders.be/"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="We want more girls in IMD!"/>
    <meta property="og:image" content="http://girlsinimd.nielsmeulders.be/img/fb-opengraph.png"/>
    <meta property="og:description" content="Girls in IMD campagne."/>
    <meta property="fb:app_id" content="966242223397117"/>
    
    <!--TWITTER CARDS-->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@weareimd">
    <meta name="twitter:title" content="Girls in IMD">
    <meta name="twitter:description" content="Girls in IMD campagne.">
    <meta name="twitter:image" content="http://girlsinimd.nielsmeulders.be/img/fb-opengraph.png">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link href='https://fonts.googleapis.com/css?family=Yellowtail|Lato:300,700,900' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <!--HEADING-->
    <section class="heading">
        <div class="container-fluid">
            <div class="text-center">
                <img class="logo-header" src="img/logo.png" alt="logo">
            </div>
            <div class="container page-title">
                <h1 class="heading1">Be Creative With Yourself</h1>
                <h1 class="heading2">&amp; ON THE WEB</h1>
                <div class="line-heading"></div>
            </div>
        </div>
    </section>

    <!--FORM-->
    <section class="form">
        <div class="container">
            <div class="text-center">
                <h2>&#35;GirlslikeIMD</h2>
                <p>Vul onderstaand formulier in voor een cursus Photoshop/Illustrator
                    <br> of leer je eigen website maken.</p>
                <p>Deze cursussen worden gegeven door onze eigen IMD-girls.
                    <br> Als zij dit kunnen, kan jij dat ook!</p>

                <form method="post" action="">
                    <div class="form-group">
                        <input type="text" class="" id="name" name="name" placeholder="NAAM">
                    </div>
                    <div class="form-group">
                        <input type="email" class="" id="email" name="email" placeholder="E-MAILADRES">
                    </div>
                    <input type="radio" name="keuze" value="1"> <span class="radio-text">Ik wil mijn eigen website maken!</span>
                    <br>
                    <input type="radio" name="keuze" value="2"> <span class="radio-text">Ik wil Photoshop leren!</span>
                    <br>
                    <button type="submit" class="">Verzenden</button>
                </form>
            </div>
        </div>
    </section>

    <!--MOVIE-->
    <section class="movie">
        <video controls>
            <source src="video/video.mp4" type="video/mp4">
        </video>
    </section>

    <!--TESTIMONIALS-->
    <section class="testimonials text-center">
        <div class="container">
            <h2>Referenties</h2>
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="person">
                        <div class="image"><img class="img-circle img-responsive" src="img/placeimg-girl1.png" alt=""></div>
                        <h4>Lola van Bommel</h4>
                        <h4>Designer</h4>
                        <p>Ik ben een 23 jarige enthousiaste teamplayer bij Boondoggle met een passie voor grafisch ontwerp en webdesign. In IMD (Interactive Multimedia Design) heb ik me kunnen ontplooien tot een grafische duizendpoot met toch een technische achtergrond.</p>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="person">
                        <div class="image"><img class="img-circle img-responsive" src="img/placeimg-girl2.jpg" alt=""></div>
                        <h4>Nele Rijnders</h4>
                        <h4>Developer</h4>
                        <p>Ik werk momenteel enkele jaren als php-developer bij TheseDays. Dat is een programmeertaal die ik geleerd heb in de richting IMD. Ik ben in staat om samen met mijn team geweldige web-applicaties te bouwen maar ook te designen aangezien we beide aspecten uitgebreid hebben geleerd binnen de opleiding.</p>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>       
        </div>
    </section>
    
    <section class="movie text-center">
        
        <h2>Wil je meer informatie over onze richting?</h2>
        
        <p class="bezoek">Bezoek dan zeker de officiële website van Thomas More</p>
        
        <a class="meer-info" href="http://www.thomasmore.be/interactive-multimedia-design-imd">Meer info</a>
        
    </section>

    <!--FOOTER-->
    <section class="footer">
        <ul class="social-media list-inline">
            <li><a href="https://www.weareimd.be" target="_blank"><i class="fa fa-globe fa-lg"></i></a></li>
            <li><a href="https://www.facebook.com/WeAreIMD" target="_blank"><i class="fa fa-facebook fa-lg"></i></a></li>
            <li><a href="https://twitter.com/weareimd" target="_blank"><i class="fa fa-twitter fa-lg"></i></a></li>
        </ul>
        <p>&copy; 2016 IMD Boys</p>
    </section>

    <!--JS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

</body>
</html>