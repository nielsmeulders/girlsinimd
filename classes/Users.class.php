<?php

    include_once("Db.class.php");

    class Users
    {

        private $m_sEmail;
        private $m_sNaam;
        private $m_iOptie;

        public function __set($p_sProperty, $p_vValue)
        {
            switch ($p_sProperty) {
                case 'Email':
                    if ($p_vValue != "")
                    {
                        $this->m_sEmail = $p_vValue;
                    }
                    else
                    {
                        throw new Exception("Geef een email adres in!");
                    }
                    break;

                case 'Naam':
                    if ($p_vValue != "")
                    {
                        $this->m_sNaam = $p_vValue;
                    }
                    else
                    {
                        throw new Exception("Geef een naam in!");
                    }
                    break;

                case 'Optie':
                    if ($p_vValue != "")
                    {
                        $this->m_iOptie = $p_vValue;
                    }
                    else
                    {
                        throw new Exception("Geef een optie in!");
                    }
                    break;
            }
        }

        public function __get($p_sProperty)
        {
            switch($p_sProperty)
            {
                case 'Email':
                    return($this->m_sEmail);
                    break;

                case 'Naam':
                    return($this->m_sNaam);
                    break;

                case 'Optie':
                    return($this->m_iOptie);
                    break;
            }
        }


        public function Create()
        {
            // this function should save the order to the database
            $conn = Db::getInstance();
            // errors doorsturen van de database
            // $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $statement = $conn->prepare('INSERT INTO emails ( email,
                                                             naam,
                                                             optie)
                                                  VALUES  ( :email,
                                                            :naam,
                                                            :optie
                                                            )');

            /*$statement = $conn->prepare('INSERT INTO girls ( email,
                                                             naam,
                                                             optie)
                                                  VALUES  ( :email,
                                                            :naam,
                                                            :optie
                                                            )');*/

            $statement->bindValue(':email',$this->Email);
            $statement->bindValue(':naam',$this->Naam);
            $statement->bindValue(':optie',$this->Optie);
            $statement->execute();
        }

        public function GetAll()
        {
            // this function should get all the orders from the database
            $conn = Db::getInstance();
            $allemails = $conn->query("SELECT * FROM emails ORDER BY id");
            //$allemails = $conn->query("SELECT * FROM girls ORDER BY id");
            return $allemails;
        }



    }



?>